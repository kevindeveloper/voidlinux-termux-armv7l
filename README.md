# VoidLinux-Termux-armv7l

Fork of AnLinux's script to install Void Linux armv7l inside Termux via proot, with a recent rootfs and base-system instead of base-bootstrap, in order to avoid XBPS's libcrypto error.